package main

import (
	"time"
)

// Server ... Defines a struct that represents the server table, as well as json tags for marshaling
type Server struct {
	ID           int       `json:"id"`
	Address      string    `json:"address"`
	Hostname     string    `json:"hostname"`
	Description  string    `json:"description"`
	CreationDate time.Time `json:"creation_date"`
	LastUpdate   time.Time `json:"last_update"`
	UpdatedBy    string    `json:"updated_by"`
}

/* JWT auth0 provided data structs*/
type Response struct {
	Message string `json:"message"`
}

// Jwks ... JSONWebkey slice
type Jwks struct {
	Keys []JSONWebKeys `json:"keys"`
}

// JSONWebKeys ... JWT
type JSONWebKeys struct {
	Kty string   `json:"kty"`
	Kid string   `json:"kid"`
	Use string   `json:"use"`
	N   string   `json:"n"`
	E   string   `json:"e"`
	X5c []string `json:"x5c"`
}
