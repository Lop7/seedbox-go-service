package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
	"time"
)

func find(w http.ResponseWriter, r *http.Request) {
	// TODO: ID  Validation!
	reqVars := mux.Vars(r)
	w.Header().Set("Content-Type", "application/json")
	serverID, errR := strconv.Atoi(reqVars["id"])
	server := Server{ID: serverID}
	if errR != nil {
		panic("GET error!")
	}
	if err := Db.Where(&server).First(&server).Error; err != nil {
		// error handling... Usually if they can't find a record
		log.Printf("%v : %v \n", time.Now().In(loc), err)
		fmt.Fprintf(w, `{"err":"Something went wrong!!"}`)
		return
	}
	json.NewEncoder(w).Encode(server)

}
func list(w http.ResponseWriter, r *http.Request) {

	var servers []Server
	w.Header().Set("Content-Type", "application/json")
	// handle any errors
	if err := Db.Find(&servers).Error; err != nil {
		log.Printf("%v : %v \n", time.Now().In(loc), err)
		fmt.Fprintf(w, `{"err":"Something went wrong!!"}`)
		return
	}
	json.NewEncoder(w).Encode(servers)

}

// create ... Creates indicated server via the ID parameter
func create(w http.ResponseWriter, r *http.Request) {
	// TODO: Object/req Validation!

	decoder := json.NewDecoder(r.Body)
	server := Server{}
	err := decoder.Decode(&server)
	if err != nil {
		fmt.Fprintf(w, "Decode error")
		log.Println(err)
		return
	}
	server.CreationDate = time.Now().In(loc)
	server.LastUpdate = time.Now().In(loc)
	Db.Create(&server)
	fmt.Fprintf(w, "Created")
}

// update ... Updates indicated server via the ID parameter, takes json as the request Body, and decodes it into Model
func update(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	decoder := json.NewDecoder(r.Body)
	updatedInfo := Server{}
	err := decoder.Decode(&updatedInfo)
	if err != nil {
		fmt.Println("Decode error")
		fmt.Println(err)
		return
	}
	serverID, errR := strconv.Atoi(params["id"])
	server := Server{ID: serverID}
	if errR != nil {
		panic("GET error!")
	}
	if err := Db.Where(&server).First(&server).Error; err != nil {
		// error handling... Usually if they can't find a record
		log.Printf("%v : %v \n", time.Now().In(loc), err)
		fmt.Fprintf(w, `Something went wrong!!`)
		return
	}
	// TODO: Add last updated by
	server.Address = updatedInfo.Address
	server.Hostname = updatedInfo.Hostname
	server.Description = updatedInfo.Description
	server.LastUpdate = time.Now().In(loc)
	server.UpdatedBy = updatedInfo.UpdatedBy
	Db.Save(&server)
	fmt.Fprintf(w, `Updated`)
}

// delete ... Deletes indicated server via the ID parameter
func delete(w http.ResponseWriter, r *http.Request) {
	reqVars := mux.Vars(r)
	serverID, errR := strconv.Atoi(reqVars["id"])
	server := Server{ID: serverID}
	if errR != nil {
		panic("DELETE error!")
	}
	if err := Db.Where(&server).First(&server).Error; err != nil {
		// error handling... Usually if they can't find a record
		log.Printf("%v : %v \n", time.Now().In(loc), err)
		fmt.Fprintf(w, `Something went wrong!!`)
		return
	}
	Db.Delete(&server)
	fmt.Fprintf(w, "Deleted")
}

// home ... Returns a welcome message
func home(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to our service!")
	return
}
