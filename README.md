
# Seedbox Test Project

#### Welcome!

This is a web service written to specifically handle calls from our other web application [written with PHP and Laravel. ](https://bitbucket.org/Lop7/seedbox-laravel-client-web-app/src/master/) I will give a quick explanation on how to get this web service up and running.

  

#### Web service / application flow

![Service Flow](https://s22.postimg.cc/u0xhvwpr5/service-flow.png)

  
  

# Requirements

### Here is what you will need to get this service running

  

- Golang 1.10+

- MySQL Server

- An OAuth2 service account (*you can use my .env file, which includes my service credentials*)

- A Machine (*I am currently running this with Windows*)

  

## Installation

*Assuming you have the above requirements...*

1. Clone this repository, and download the .env file I attached to the e-mail. Feel free to create your own Environment Vars if you are comfortable with auth0.

2. Download the following custom packages (*dependencies*) via these Glide

- Install [Glide](https://github.com/Masterminds/glide) via curl `curl https://glide.sh/get | sh` if you have a unix based machine, otherwise follow Windows instructions in the link.

- Install dependences using `glide install` while within the project root

3. (**optional, if step 2 did not work**) Or manually install dependencies

```shell
$ go get github.com/go-sql-driver/mysql
$ go get github.com/gorilla/mux
$ go get github.com/jinzhu/gorm
$ go get github.com/jinzhu/gorm/dialects/mysql
$ go get github.com/joho/godotenv/autoload
$ go get github.com/urfave/negroni
$ go get github.com/auth0/go-jwt-middleware
$ go get github.com/dgrijalva/jwt-go
$ go get github.com/joho/godotenv/autoload
```

  

## Run or Build

  

- Move into the directory where you cloned this repository

- Run the following command in your shell

```shell

$ go build service.go models.go jwtUtilities.go controllers.go && ./service.exe

# Omit the .exe if you are running on a Unix based machine

```

The web service should be up and running now.

## Why Go?

  

I like Go, it is quick and easy to pickup. I had not really written any type of web service with Go, so it was an interesting process.

  

## How does the service work, anyway?

  

The service is powered by JSON Web Tokens (JWT) and the OAuth2. Essentially, this web service takes a request object that includes a JWT, as well as requests for data from the web service. This web service parses the JWT and verifies each part of the token for just about every request we send to it. If the JWT has maintained its integrity, we grant access to the service data. To read more about Web Tokens and Grant flows, check out [Auth0](https://auth0.com/) they have excellent documentation.

  

## What's next?

  

CI and build deployments. Unfortunately I have been busy as of late, so this may have to wait until this weekend (<i>it is 4 AM as I type</i>). I typically test as well, but I wanted to meet the deadline given to me.

# Please check out the sister repo that goes along with this service

### [Bitbucket link](https://bitbucket.org/Lop7/seedbox-laravel-client-web-app/src/master/)

### Also check out my last mini project with a pretty build

[GitHub link](https://github.com/masonmr/small-ecometrica-demo)