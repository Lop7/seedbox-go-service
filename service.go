package main

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/joho/godotenv/autoload"
	"github.com/urfave/negroni"
	"log"
	"net/http"
	"os"
	"time"
)

// Db ... shared db resource, can access it in all CRUD functions
var Db *gorm.DB

// loc ... Gets EastCoast timezone
var loc, _ = time.LoadLocation("EST")
var PORT string

// initDB ... Gets instance of database connection via Gorm
func initDB() {
	var err error
	connString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true",
		os.Getenv("DB_USERNAME"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_DATABASE"))
	Db, err = gorm.Open(os.Getenv("DB_CONNECTION"), connString)
	if err != nil {
		panic(err)
	}
}

// initRouter ... Begins router with gomux. Negroni is used to verify JWT as middleware.
func initRouter() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", home)
	// Get auth0's middleware function / object
	jwtMiddleware := getJWTMiddleware()
	subRouter := mux.NewRouter().PathPrefix("/auth").Subrouter().StrictSlash(true)
	subRouter.HandleFunc("/servers", list).Methods("GET")
	subRouter.HandleFunc("/servers", create).Methods("POST")
	subRouter.HandleFunc("/servers/{id}", update).Methods("PUT")
	subRouter.HandleFunc("/servers/{id}", find).Methods("GET")
	subRouter.HandleFunc("/servers/{id}", delete).Methods("DELETE")
	// Bind JWT verification middleware to /auth prefix
	router.PathPrefix("/auth").Handler(negroni.New(
		negroni.HandlerFunc(jwtMiddleware.HandlerWithNext),
		negroni.Wrap(subRouter),
	))
	PORT = os.Getenv("PORT")
	if len(PORT) == 0 {
		PORT = os.Getenv("LISTEN_PORT")
	}

	log.Printf("Listening on %s \n ", PORT)
	log.Fatal(http.ListenAndServe(":"+PORT, router))
}

// main ... Where everything starts
func main() {
	initDB()
	// Very cool Gorm method that migrates a Struct design into our DB.
	Db.AutoMigrate(&Server{})
	initRouter()
	defer Db.Close()
}
